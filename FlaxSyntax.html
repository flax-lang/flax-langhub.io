<div class="docs">
<header><h1>Flax Language Syntax</h1></header>
<p>Note that the language is still in flux, and syntax elements are subject to change</p>

<hr />

<section>

<header id="doc-builtin-types"><h2>Builtin Types</h2></header>
<p>

Out of the 14 builtin types that Flax has, 12 are POD types (plain-old-data). 10 of them are integer types, while the other 2 are floating-point types. Their names should convey their semantics clearly, but in case it does not, the excerpt below exemplifies.

<pre class="dcodesegment"><code class="lang-swift dcsinner">
val i8: Int8		// -128                       to 127
val u8: Uint8		// 0                          to 255
val i16: Int16		// -32,768                    to 32,767
val u16: Uint16		// 0                          to 65,535
val i32: Int32		// −2,147,483,648             to 2,147,483,647
val u32: Uint32		// 0                          to 4,294,967,295
val i64: Int64		// −9,223,372,036,854,775,808 to 9,223,372,036,854,775,807
val u64: Uint64		// 0                          to 18,446,744,073,709,551,615

val f32: Float32	// single-precision float
val f64: Float64	// double-precision float
</code></pre>
</p>

<p>
The string is also a builtin type, unlike other low-level languages such as C/C++. String literals enclosed in <span class="inline-code">" "</span> have a type of <span class="inline-code">String</span>, not <span class="inline-code">Int8*</span>. Actually, to be completely accurate, <span class="inline-code">String</span> is not exactly a builtin type, as it is completely defined in Flax code. However, the compiler will, instead of creating the typical <span class="inline-code">const char*</span> for a string literal, create a <span class="inline-code">String</span> instance with a pointer to the read-only string. Any modifications to this string will cause a heap allocation and a memcpy.

<pre class="dcodesegment"><code class="lang-swift dcsinner">
var foo = "Hello"				// type: String
var bar = foo + ", World!"		// creates new String for 'bar'
</code></pre></p>

<p>
Internally, a <span class="inline-code">String</span> is simply a list of bytes. However, it fully supports Unicode using the UTF-8 encoding format. The <span class="inline-code">.length</span> field of a <span class="inline-code">String</span> instance will reflect the number of codepoints in the string as opposed to be number of bytes. For purely ASCII-based strings, the length remains the same.
</p>

<p>One final note about <span class="inline-code">Strings</span>: they're completely optional. By not having <span class="inline-code">import Foundation</span> in your code, you will not import the definition for the <span class="inline-code">String</span> class. Under these circumstances, the compiler will instead emit the traditional <span class="inline-code">const char*</span> for string literals. You are free to define your own string class that, for instance, does not touch the heap.
</p>

<p>
Note that integer literals have a type of <span class="inline-code">Int64</span>.
However, the compiler will automatically cast them into an appropriate type for the expression, if the destination type is big enough. An error will be thrown if, for instance, you are trying to do this: <span class="inline-code">var x: Int8 = 400</span>
</p>

<p>
As an aside, <span class="inline-code">Int</span> and <span class="inline-code">Uint</span> are simply aliases for <span class="inline-code">Int64</span> and <span class="inline-code">Uint64</span> respectively. They are not treated as distinct types, and in fact are aliased in the very early stages of code generation.
</p>


<hr />
<header id="doc-vars"><h2>Variables</h2></header>
<p>
Variables are declared using a Pascal-like syntax, where the type comes after the variable identifier. Type inference is also supported -- this however requires that the variable be given an initial value at its declaration site.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var foo: Int64 = 91		// explicit type
var bar = 591			// implicit type (Int64)
var qux					// illegal, cannot infer type
</code></pre>
</p>

<p>
When no explicit initialiser is given, variables are initialised to a zero value that is appropriate. Pointers and integers get 0, while aggregated types (structs, classes) will have their <span class="inline-code">__automatic_init()</span> function called (explained below), unless explicitly specified with an attribute.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var qux: Int64			// implicit 'qux = 0'
</code></pre>
</p>


<p>
Constants can be declared using the <span class="inline-code">val</span> keyword in place of <span class="inline-code">var</span>. As the name suggests, they cannot be changed once set, and must have a value at initialisation.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
val qux = 501
qux = 49134		// illegal
</code></pre>
</p>






<hr />
<header id="doc-pointers"><h2>Pointers</h2></header>
<p>
Flax fully supports pointers, and they have an intuitive syntax. Note that unlike in C/C++, the "pointerness" of a type is bound to the type itself, not the name. It would be pretty stupid to have <span class="inline-code">var foo*: Int32</span> for an <span class="inline-code">Int32*</span>, would it not?
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var vga: Int16* = 0xB8000 as Int16*
</code></pre></p>


<p>
Taking the address of a variable, as well as dereferencing a pointer, have an easy-to-remember syntax as well:
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var firstByte: Int16 = #vga				// dereference with '#'
var vgaPtrAgain: Int16* = &amp;firstByte	// take the address with '&amp;'
</code></pre>
</p>


<p>
Naturally, pointers can be indexed into. No bounds-checking will be done, however -- it is planned.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var foos: Int64* = alloc[4] Int64		// alloc explained below
var foo: Int64 = foos[1]
</code></pre>
</p>

<p>
Additionally, pointer arithmetic is also supported. That is to say, adding or subtracting an integer from a pointer type will in fact add or subtract from the address (stored in the pointer var) by that number multiplied by the size in bytes of the type pointed to. If that sounds confusing, the example below will help:
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var qux: Int64* = 0xFF00 as Int64*
qux += 4				// actually (qux as UintPtr) += (4 * sizeof(Int64))

printf("%p", qux)		// prints 0xFF20
</code></pre>
</p>



<hr />
<header id="doc-functions"><h2>Functions</h2></header>
<p>
Functions are declared with the <span class="inline-code">func</span> keyword, proceeded by the function name, a list of arguments in parentheses, and finally the return type after a <span class="inline-code">-&gt;</span>. The return type can be omitted if the function returns Void (aka nothing).

<pre class="dcodesegment"><code class="lang-swift dcsinner">
func Foo(bar: Int64, qux: Int8*) -> Float64
{
	// ...
}
</code></pre>
</p>

<p>
Naturally, the <span class="inline-code">return</span> keyword can be used to return a value from a function. Note that it is an error not to return a value in a function with a non-Void return value.
<br />
Implicit returns are also supported -- if the type of the last expression in a function matches its return type, it will be treated as the implicit return value. It is most useful for single-line functions like the one below. Alternative, simpler syntax forms are being explored.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
func mult(a: Float32, b: Float32) -> Float32 { a * b }
</code></pre>
</p>

<p>
Flax supports calling C calling conventions (due to using LLVM), and as such two-way interaction is possible. To declare an externally-defined C function, use the <span class="inline-code">ffi</span> keyword preceeding <span class="inline-code">func</span>.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
ffi func printf(x: Int8*, ...)
</code></pre>

<br />
As you can see, declaring C functions that take a variable number of arguments is supported using <span class="inline-code">...</span>, however it is currently not possible to write Flax functions with a variable number of parameters. Additionally, <span class="inline-code">ffi</span> functions are the only time where Flax functions can have no body. Finally, names still need to be given to all the parameters in the declaration -- this will be addressed eventually.
</p>

<p>
Functions can be overloaded if they take a different number of arguments, or arguments of different types between their incarnations. The Flax compiler will determine at compile time which function to call. Note that if two functions differ only in their return type, they cannot be overloaded, as the compiler usually does not have enough information to determine which function to call in most cases.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
func printInt(x: Int8)    { printf("%hhd is an Int8!\n", x) }
func printInt(x: Int32)   { printf("%d is an Int32!\n", x)  }
func printInt(x: Float32) { printf("%f is a Float32!\n", x) }

printf(42 as Int8)		// 42 is an Int8
printf(581 as Int32)	// 581 is an Int32
printf(45.5 as Float32)	// 45.5 is a Float32
</code></pre>
</p>






<hr />
<header id="doc-structs"><h2>Structs</h2></header>
<p>
Structs are declared with the <span class="inline-code">struct</span> keyword.
All functions in a struct have an implicit first parameter called 'self', which is a pointer type.
Flax does not distinguish between pointer access and non-pointer access, both use the '.' operator. Yes, this means that accessing struct members through a pointer is allowed (in C/C++, this is done using <span class="inline-code">-&gt;</span>)

<pre class="dcodesegment"><code class="lang-swift dcsinner">
struct FooBar
{
	// ... members ...
}
</code></pre>
</p>


<p>
Structs can have 4 kinds of members: constructors and destructors, member variables, computed properties, and functions (although theoretially init functions are still functions). Custom constructors can be specified, and can be overloaded -- the appropriate one is chosen based on the arguments passed. However, currently destructors are not yet supported.
<br />
<pre class="dcodesegment"><code class="lang-swift dcsinner">
struct GuideToGalaxy
{
	var theAnswer: Int64
	var billionsOfPeopleOnEarth: Int64 = 7	// inline initialisers are allowed.

	// You can declare an init() function yourself
	init()
	{
		self.theAnswer = self.billionsOfPeopleOnEarth * 6
	}

	func getAnswer() -> Int64
	{
		return self.theAnswer
	}
}
</code></pre>
<br />
<p>
If you remember the <span class="inline-code">__automatic_init()</span> previously mentioned, here is where it comes into play: it is called to ensure a zero-value on all member variables in a struct instance, as well as to actually assign the inline initialiser values. To disable this for whatever reason (and to leave it uninitialised), use the <span class="inline-code">@noautoinit</span> attribute. Note that this only applies to struct types, and has no effect on variables with a primitive type.
</p>

<p>
Of course, structs are valid types everywhere, and can be used to define variables. Note that the struct definition does not need to preceed any usage of it, because the compiler takes multiple passes.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var guide1: GuideToGalaxy
var guide2: GuideToGalaxy* = &amp;guide1
</code></pre>
</p>

<p>
In order to initialise struct objects with a non-zero number of parameters (ie. call init(...)), you can use either of the two syntax forms below, which are equivalent (Note that in the example below, String is a Flax Foundation struct type). Of course, you can still manually specify the type, but that just ends up being redundant, since you already specify the type name in calling the constructor.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var str: String("Hello, World")
var str2 = String("This works the same way")
</code></pre>
</p>

<p>
To access members of structs, be it variables or functions, use the <span class="inline-code">.</span> operator. For convenience when working with structs in memory (at a low level), struct-pointers can also be accessed with the dot syntax, as mentioned above, without explicitly dereferencing the variable.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var answer: Int64 = guide2.calculateAnswer()	// call directly on pointer
guide1.billionsOfPeopleOnEarth -= 4				// asteroid?
</code></pre>
</p>



<hr />
<header id="doc-arrays"><h2>Static Functions</h2></header>
<p>
Member functions (methods) can be marked with the <span class="inline-code">static</span> keyword, to indicate that they should be called without needing an instance of the object they belong to. As such, they are accessed using dot-syntax on the name of the type itself, instead of an instance variable. As a sidenote, they can be called on the <span class="inline-code">self</span> instance as well.

<pre class="dcodesegment"><code class="lang-swift dcsinner">
struct Bar
{
	static func doStaticThings() -> Int
	{
		println("Things of a static nature")
		return 58
	}
}

// ---------
val staticResult = Bar.doStaticThings()

val bar: Bar
bar.doStaticThings()		// also legal, the value of 'bar' is not used

</code></pre>
</p>

<hr />
<header id="doc-arrays"><h2>Nested Types</h2></header>
<p>
You can declare types inside another type, and they function intuitively. They can be qualified with the <span class="inline-code">private</span> access attribute to make them visible only to members of the enclosing class.

<pre class="dcodesegment"><code class="lang-swift dcsinner">
struct Bar
{
	var norf: Int = 1600
	var yorg: Int = 5000
	var goop: Int = 6810

	struct Nested
	{
		var g: Int = 500

		struct Nest2
		{
			var m: Int = 230
			func doStuff() -> Int
			{
				println("Doing some stuff...")
				return m * 7
			}

			static func nestedStaticFunction() -> Float
			{
				return 3.141593
			}
		}

		func doStuff() -> Int
		{
			printf("in stuff(): self is %x\n", self)
			return g / 3
		}
	}
}

</code></pre>
</p>


<p>
Since the struct definitions for <span class="inline-code">Nested</span> and <span class="inline-code">Nest2</span> are publicly accessible (by default), you can declare variables of that type.

<pre class="dcodesegment"><code class="lang-swift dcsinner">
val foo: Bar.Nested
printf("[%d]\n", foo.doStuff())
</code></pre>
</p>

<p>
Static functions inside nested types are accessed using dot-syntax as well, in an intuitive manner.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
val result = Bar.Nested.Nest2.nestedStaticFunction()
printf("%f\n", result)			// 3.14159263
</code></pre>
</p>







<hr />
<header id="doc-arrays"><h2>Arrays</h2></header>
<p>
Currently, only fixed-length arrays are supported in Flax. These arrays have compile-time bounds checking, and will generate an error if an attempt is made to access memory outside its allocated length. When creating an array in Flax, all elements will get their default value, usually zero -- unless the <span class="inline-code">@noautoinit</span> attribute is specified.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var i64Arr: Int64[100]
var something = i64Arr[40]	// okay
i64Arr[591] = 58774			// error
</code></pre>
</p>



<hr />
<header id="doc-ifelse"><h2>If/Else</h2></header>
<p>
Flax supports the ubiquitous if/elseif/else construct, and its syntax is quite unimaginative, especially to users of other programming languages. A few special points to note -- firstly, the condition to evalulate can be a non-boolean type, in which case a comparison is made in the form of <span class="inline-code">!= 0</span>, meaning all other values evaluate as <span class="inline-code">true</span>. Secondly, the parentheses after the if statement are optional. Finally, the implicit braces rule of C/C++ is not supported -- all if/else statements must be surrounded in braces.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
if someCondition
{
	// ...
}
else if someOtherCondition &amp;&amp; today != "tuesday"
{
	// ...
}
else
{
	// ...
}
</code></pre>
</p>

<p>
Both the <span class="inline-code">&amp;&amp;</span> and the <span class="inline-code">||</span> boolean operators are short-circuiting. To those unfamiliar, this means that for an expression <span class="inline-code">a &amp;&amp; b</span>, <span class="inline-code">b</span> will never be evaluated (or if it is a function that returns a boolean, it will never be called) if <span class="inline-code">a</span> is false.
<br />
Conversely, for <span class="inline-code">a || b</span>, if <span class="inline-code">a</span> is true, <span class="inline-code">b</span> will never be evaluated.
</p>



<hr />
<header id="doc-loops"><h2>Loops</h2></header>

<p>
Flax currently supports 3 main kinds of loops; the <span class="inline-code">while</span> loop, the <span class="inline-code">do-while</span> loop and the infinite loop. Note that implicit braces are not permitted as well for these. They have the following syntax:
<pre class="dcodesegment"><code class="lang-swift dcsinner">
while someCondition
{
	// do stuff while someCondition is true
}

do {
	// do stuff at least once, then only while someCondition is true
} while(someCondition)

loop {
	// keep doing stuff
}
</code></pre>
</p>

<p>
As an aside, blocks without any keyword (eg. <span class="inline-code">{ printf("Hi") }</span>) will not be executed. To ensure that such a block is evaluated, preceed the block with either <span class="inline-code">do</span> or <span class="inline-code">loop</span>, depending on the effect you wish to achieve. An interesting tidbit is that all 3 of the above loops share the same AST node, and as such, you can do interesting things with them:
<pre class="dcodesegment">
<code class="lang-swift dcsinner">
do {
	// executed once
}

loop {
	// infinite loop
}

do {
	// execute while foo != 0
} while(foo)

loop {
	// same as do ... while(foo)
} while(foo)
</code></pre>
</p>









<hr />
<header id="doc-mangling"><h2>Name Mangling</h2></header>
<p>
There are two main types of name mangling in Flax, mangling of function names and the mangling of function names as members of structs. In both cases, mangling results in a rather long final name, but the benefit of this is that it maintains some sense of readability as to the signature of the function.
<br />
<br />
<br />


<header><h3>Functions</h3></header>
To handle outputting LLVM bitcode (which does not support overloading), Flax function names are mangled, unless specified otherwise. It uses its own cranky mangling system, observe: <span class="inline-code">[basename]#_[param1Type]_[paramNType]</span>
<br />
If a function takes no parameters, it is mangled as if it has a single parameter of type <span class="inline-code">Void</span>. To specify that a function should not be mangled, use the <span class="inline-code">@nomangle</span> attribute. Attributes will be discussed in detail below.
<br />
<br />
<br />
<br />



<header><h3>Functions in Structs</h3></header>
The mangling of functions in struct types is very simple compared to the actual mangling of function names; the name of the struct is essentially just prepended to the mangled name of the function: <span class="inline-code">__struct#[structName]_[mangledFuncName]</span>
</p>


<hr />
<header id="doc-alloc"><h2>Alloc and Dealloc</h2></header>
<p>
Flax has the <span class="inline-code">alloc</span> and <span class="inline-code">dealloc</span> keywords -- these are used to allocate and deallocate memory on the heap respectively. Currently they are fixed to calling <span class="inline-code">malloc()</span> and <span class="inline-code">free()</span>, although it is planned to be able to specify a custom name for these functions.
</p>

<p>
To use <span class="inline-code">alloc</span>, place it on the right-hand side of an assignment statement. You can specify the number of elements to reserve space for, as well as pass arguments to the type's initialiser if it has one. As you might guess, <span class="inline-code">alloc</span> returns a pointer to the type and <span class="inline-code">dealloc</span> expects one as well.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var foo = alloc String				// 1 String
var bar = alloc[4] String			// 4 Strings
var qux = alloc[8] String("flob")	// 8 Strings and set them all to "flob"
</code></pre>
</p>

<p>
<span class="inline-code">dealloc</span> is used in a similar way, although it obviously cannot be assigned to anything. Simply write the name of the variable to deallocate after the <span class="inline-code">dealloc</span> keyword. Note that the compiler currently does not check whether what you are deallocating was actually allocated using <span class="inline-code">alloc</span>, and as such a bogus deallocation will trigger a runtime abort in your libc implementation of <span class="inline-code">free()</span>.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
dealloc foo
dealloc bar
dealloc qux
</code></pre>
</p>

<p>
What the compiler does do, however, is check for use-after-free scenarios. Note that it is currently very basic -- it might both trigger false positives, and fail to catch some instances. Nonetheless, it does a decent job.
<pre class="dcodesegment"><code class="lang-swift dcsinner">
var norf = alloc Int64
#norf = 400
dealloc norf

printf("norf is %lld\n", norf)		// generates warning
</code></pre>
</p>



<hr />
<header id="doc-alloc"><h2>Deferred Statements</h2></header>
<p>
Deferred statements are declared using the <span class="inline-code">defer &lt;expr&gt;</span> syntax, where the expression can be anything. Note that while any expression can be deferred, including the declaration of types, arithmetic operations, as well as variable declarations, it does not make sense to use it for such purposes, even though the compiler will accept it.

Deferment operates on the braced-block scopes. Functions, If-else bodies and closures are all examples of braced-block scopes. When an expression is deferred, it will only be evaluated when the block closes, after the rest of the block.

Note that all deferred expressions will be evaluated before any return is executed.

<pre class="dcodesegment"><code class="lang-swift dcsinner">
func foo()
{
	defer printf("second")
	printf("first, ")

	// prints "first, second"
}
</code></pre>
</p>

<p>
When multiple expressions are deferred in a block, they are evaluated in the reverse order that they were declared. As such, it is comparable to a stack structure.

<pre class="dcodesegment"><code class="lang-swift dcsinner">
func foo()
{
	defer printf("04")
	defer printf("03, ")
	defer printf("02, ")
	printf("01, ")

	// prints "01, 02, 03, 04"
}
</code></pre>
</p>

<p>
As previously mentioned, anything is deferrable. The examples below are completely legal, and will function exactly as expected:
<pre class="dcodesegment"><code class="lang-swift dcsinner">

func foo() -> Int
{
	defer i += 58

	var i = 27
	return i		// returns 85
}

// ---------------
// no more ):

</code></pre>
</p>








</section>
</div>

























